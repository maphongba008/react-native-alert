
# react-native-alert

## Getting started

`$ npm install react-native-alert --save`

### Mostly automatic installation

`$ react-native link react-native-alert`

### Manual installation

#### iOS

1. In XCode, in the project navigator, right click `Libraries` ➜ `Add Files to [your project's name]`
2. Go to `node_modules` ➜ `react-native-alert` and add `RNReactNativeAlert.xcodeproj`
3. In XCode, in the project navigator, select your project. Add `libRNReactNativeAlert.a` to your project's `Build Phases` ➜ `Link Binary With Libraries`
4. Run your project (`Cmd+R`)

#### Android

1. Open up `android/app/src/main/java/[...]/MainActivity.java`
  - Add `import com.reactlibrary.RNAlertPackage;` to the imports at the top of the file
  - Add `new RNAlertPackage()` to the list returned by the `getPackages()` method
2. Append the following lines to `android/settings.gradle`:
  	```
  	include ':react-native-alert'
  	project(':react-native-alert').projectDir = new File(rootProject.projectDir, 	'../node_modules/react-native-alert/android')
  	```
3. Insert the following lines inside the dependencies block in `android/app/build.gradle`:
  	```
      compile project(':react-native-alert')
  	```


### iOS: Require 1 extra step for both Auto and Manual installation

1. Open xcode/<Your project name>/Libraries/RNReactNativeAlert
2. Drag and drop Resources.bundle into Build Phases -> Copy bundle Resources 

## Usage
```javascript
import Alert from 'react-native-alert';

Alert.show('Here is the title', 'My message', [
  {
    text: 'Submit',
    textColor: '#FF0022',
    onPress: (text) => alert(text), // text from the input
  },
  {
    text: 'Cancel',
    textColor: '#00FF22',
  },
], {
    cancelable: false,
    horizontal: false, // force buttons to render vertical
    fontPath: 'assets/fontA.ttf', // fonts for android must place in assets, for iOS, it's font name
    hasInput: false, // render input in an alert
    iconName: null, // custom icon, must place in Images.xcassets (iOs) or drawable (android)
    inputHolder: '',
    keyboardType: 'default', // default | number | password
  });
```
  