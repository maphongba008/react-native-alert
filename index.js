import { NativeModules } from 'react-native';

type ButtonProps = {
  text: string;
  onPress?: (text: string) => void;
  fontPath?: string;
  textColor?: string;
  backgroundColor?: string;
  height?: number;
  borderRadius?: number;
  marginHorizontal?: number;
}

type Option = {
  horizontal?: boolean;
  cancelable?: boolean;
  iconName?: string;
  fontPath?: string;
  hasInput?: boolean;
  inputHolder?: string;
  keyboardType?: 'number' | 'default' | 'password';
  separatorColor?: string;
  separatorHeight?: number;

}

export default class Alert {

  static show = (title: string, message: string, buttons: Array<ButtonProps>, option: Option = {}) => {
    NativeModules.RNAlert.showAlert({
      message,
      title,
      buttons,
      ...option,
    }, (index, text) => {
      const onPress = buttons[index].onPress;
      onPress && onPress(text);
    });
  }

}
