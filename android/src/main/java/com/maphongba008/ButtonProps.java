package com.maphongba008;

import com.facebook.react.bridge.ReadableArray;
import com.facebook.react.bridge.ReadableMap;

import java.util.ArrayList;
import java.util.List;

/**
 * ReactNativeTemplate - com.reactlibrary
 * Created by Kerofrog on 2019-07-20.
 */
public class ButtonProps {

    private String text;
    private String fontPath;
    private String textColor;
    private int marginHorizontal;
    private String backgroundColor;
    private int borderRadius;
    private int height;

    static List<ButtonProps> fromArray(ReadableArray array) {
        List<ButtonProps> list = new ArrayList<>();
        if (array == null) {
            return list;
        }
        for (int i = 0; i < array.size(); i++) {
            ReadableMap map = array.getMap(i);
            list.add(new ButtonProps(map));
        }
        return list;
    }

    private ButtonProps(ReadableMap map) {
        text = MapUtils.fromMap(map, "text", null);
        textColor = MapUtils.fromMap(map, "textColor", "#000000");
        fontPath = MapUtils.fromMap(map, "fontPath", null);
        marginHorizontal =MapUtils.fromMap(map, "marginHorizontal", 0);
        backgroundColor = MapUtils.fromMap(map, "backgroundColor", null);
        borderRadius = MapUtils.fromMap(map, "borderRadius", 0);
        height = MapUtils.fromMap(map, "height", 50);
    }

    public String getText() {
        return text;
    }

    public String getFontPath() {
        return fontPath;
    }

    public String getTextColor() {
        return textColor;
    }

    public int getMarginHorizontal() {
        return marginHorizontal;
    }

    public String getBackgroundColor() {
        return backgroundColor;
    }

    public int getBorderRadius() {
        return borderRadius;
    }

    public int getHeight() {
        return height;
    }

    @Override
    public String toString() {
        return "ButtonProps{" +
                "text='" + text + '\'' +
                ", fontPath='" + fontPath + '\'' +
                ", textColor='" + textColor + '\'' +
                '}';
    }
}
