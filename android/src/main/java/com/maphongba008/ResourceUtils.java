package com.maphongba008;

import android.content.Context;
import android.content.res.Resources;

/**
 * ReactNativeTemplate - com.reactlibrary
 * Created by Kerofrog on 2019-07-20.
 */
public class ResourceUtils {

    public static int pxToDp(int px) {
        return (int) (px / Resources.getSystem().getDisplayMetrics().density);
    }

    public static int dpToPx(int dp) {
        return (int) (dp * Resources.getSystem().getDisplayMetrics().density);
    }

    public static int getDrawable(Context c, String name) {
        return c.getResources().getIdentifier(name, "drawable", c.getPackageName());
    }

}
