package com.maphongba008;

import android.app.Dialog;
import android.app.DialogFragment;
import android.graphics.Color;
import android.graphics.Typeface;
import android.graphics.drawable.PaintDrawable;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.text.InputType;
import android.util.Log;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.LinearLayout.LayoutParams;
import android.widget.TextView;

import com.facebook.react.bridge.Callback;

import java.util.List;

import static com.maphongba008.ResourceUtils.dpToPx;

/**
 * ReactNativeTemplate - com.reactlibrary
 * Created by Kerofrog on 2019-07-20.
 */
public class AlertDialogFragment extends DialogFragment implements View.OnClickListener {

    View v;

    DialogOption option;
    Callback callback;
    EditText editText;

    @Override
    public void onStart() {
        super.onStart();
        Dialog dialog = getDialog();
        if (dialog != null) {
            int width = ViewGroup.LayoutParams.MATCH_PARENT;
            int height = ViewGroup.LayoutParams.WRAP_CONTENT;
            if (dialog.getWindow() != null) {
                dialog.getWindow().setLayout(width, height);
            }

        }
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        v = inflater.inflate(R.layout.mp3_alert, null, false);
        return v;
    }

    public void update(DialogOption option, Callback callback) {
        this.option = option;
        this.callback = callback;
    }


    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        Typeface font = null;
        if (option.getFontPath() != null) {
            try {
                font = Typeface.createFromAsset(getActivity().getAssets(), option.getFontPath());
            } catch (Exception e) {
                Log.e("Error font path", e.getMessage());
            }
        }
        TextView titleText = v.findViewById(R.id.titleText);
        TextView messageText = v.findViewById(R.id.messageText);
        ImageView imageView = v.findViewById(R.id.imageView);
        editText = v.findViewById(R.id.editText);

        messageText.setText(option.getMessage());
        int iconRes = ResourceUtils.getDrawable(getActivity(), option.getIconName());
        if (iconRes != 0) {
            imageView.setImageResource(iconRes);
        } else {
            imageView.setVisibility(View.GONE);
        }
        if (option.getTitle() == null || "".equalsIgnoreCase(option.getTitle())) {
            titleText.setVisibility(View.GONE);
        } else {
            titleText.setText(option.getTitle());
        }
        if (option.isHasInput()) {
            editText.setText(option.getText());
            editText.setHint(option.getInputHolder());
            switch (option.getKeyboardType()) {
                case "number": {
                    editText.setInputType(InputType.TYPE_CLASS_NUMBER | InputType.TYPE_NUMBER_FLAG_DECIMAL);
                    break;
                }
                case "password": {
                    editText.setInputType(InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_VARIATION_PASSWORD);
                    break;
                }
            }
            editText.setTypeface(font);
        } else {
            editText.setVisibility(View.GONE);
        }
        if (font != null) {
            titleText.setTypeface(font);
            messageText.setTypeface(font);
        }
        setCancelable(option.isCancelable());
        if (option.getButtons().size() > 0) {
            if (option.isHorizontal()) {
                this.renderHorizontalButton(font);
            } else {
                this.renderVerticalButtons(font);
            }
        }
    }

    void renderHorizontalButton(Typeface font) {
        LinearLayout container = v.findViewById(R.id.buttonContainer);
        container.setOrientation(LinearLayout.HORIZONTAL);
        container.setMinimumHeight(dpToPx(50));
        container.setBackgroundColor(Color.parseColor("#e4e4e4"));
        List<ButtonProps> buttons = option.getButtons();
        for (int i = 0; i < buttons.size(); i++) {
            Button button = new Button(getActivity());
            LayoutParams params = new LayoutParams(0, LayoutParams.MATCH_PARENT, 1);
            if (i != 0) {
                params.setMargins(1, 1, 0, 0);
                button.setLayoutParams(params);
            } else {
                params.setMargins(0, 1, 0, 0);
                button.setLayoutParams(params);
            }
            stylingButton(button, buttons.get(i), font);
            button.setTag(i);
            button.setOnClickListener(this);
            container.addView(button);
        }
    }

    void renderVerticalButtons(Typeface font) {
        List<ButtonProps> buttons = option.getButtons();
        LinearLayout container = v.findViewById(R.id.buttonContainer);
        container.setOrientation(LinearLayout.VERTICAL);
        LayoutParams layoutParams = new LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.WRAP_CONTENT);
        container.setLayoutParams(layoutParams);
        for (int i = 0; i < buttons.size(); i++) {
            ButtonProps props = buttons.get(i);

            if (!option.isNoLastSeparator() || i != buttons.size() - 1) {
                View v = new View(getActivity());
                v.setBackgroundColor(option.getSeparatorColor() == null ? Color.TRANSPARENT : Color.parseColor(option.getSeparatorColor()));
                v.setLayoutParams(new LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, option.getSeparatorHeight()));
                container.addView(v);
            }

            Button button = new Button(getActivity());
            LayoutParams params = new LayoutParams(LayoutParams.MATCH_PARENT, dpToPx(props.getHeight()));
            params.setMargins(dpToPx(props.getMarginHorizontal()), 0, dpToPx(props.getMarginHorizontal()), 0);
            button.setLayoutParams(params);
            button.setTag(i);
            button.setOnClickListener(this);
            stylingButton(button, buttons.get(i), font);
            container.addView(button);

        }
    }

    void stylingButton(Button b, ButtonProps props, Typeface defaultFont) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            b.setStateListAnimator(null);
        }
        b.setText(props.getText());
        b.setTextColor(Color.parseColor(props.getTextColor()));
        b.setTextSize(TypedValue.COMPLEX_UNIT_SP, 17);
        b.setAllCaps(false);
        int bgColor = props.getBackgroundColor() == null ? Color.TRANSPARENT : Color.parseColor(props.getBackgroundColor());

        if (props.getBorderRadius() != 0) {
            PaintDrawable drawable = new PaintDrawable(bgColor);
            drawable.setCornerRadius(dpToPx(props.getBorderRadius()));
            b.setBackground(drawable);
        } else {
            b.setBackgroundColor(bgColor);
        }
        Typeface font = null;
        if (props.getFontPath() != null) {
            try {
                font = Typeface.createFromAsset(getActivity().getAssets(), props.getFontPath());
            } catch (Exception e) {
                Log.e("Error font path", e.getMessage());
            }
        }
        if (font != null) {
            b.setTypeface(font);
        } else {
            b.setTypeface(defaultFont);
        }
    }

    @Override
    public void onClick(View v) {
        try {
            int tag = (int) v.getTag();
            callback.invoke(tag, editText.getText().toString());
        } catch (Exception e) {
            Log.e("ERROR", e.getMessage());
        }
        dismiss();
    }
}
