package com.maphongba008;

import com.facebook.react.bridge.ReadableMap;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * ReactNativeTemplate - com.reactlibrary
 * Created by Kerofrog on 2019-07-20.
 */
public class DialogOption implements Serializable {

    private String fontPath;
    private String title;
    private String message;
    private boolean horizontal;
    private boolean cancelable;
    private String iconName;
    private boolean hasInput;
    private String inputHolder;
    private String keyboardType;
    private String text;
    private List<ButtonProps> buttons;
    private int separatorHeight;
    private String separatorColor;
    private boolean noLastSeparator;

    public DialogOption(ReadableMap map) {
        fontPath = MapUtils.fromMap(map, "fontPath", null);
        title = MapUtils.fromMap(map, "title", null);
        message = MapUtils.fromMap(map, "message", "");
        iconName = MapUtils.fromMap(map, "iconName", "ic_info");
        buttons = map.hasKey("buttons") ? ButtonProps.fromArray(map.getArray("buttons")) : new ArrayList<ButtonProps>();
        horizontal = map.hasKey("horizontal") ? map.getBoolean("horizontal") : buttons.size() < 3;
        cancelable = MapUtils.fromMap(map, "cancelable", false);
        hasInput = MapUtils.fromMap(map, "hasInput", false);
        inputHolder = MapUtils.fromMap(map, "inputHolder", "");
        keyboardType = MapUtils.fromMap(map, "keyboardType", "text");
        text = MapUtils.fromMap(map, "text", "");
        separatorHeight = ResourceUtils.dpToPx(MapUtils.fromMap(map, "separatorHeight", 1));
        separatorColor = MapUtils.fromMap(map, "separatorColor", null);
        noLastSeparator = MapUtils.fromMap(map, "noLastSeparator", false);
    }

    public String getFontPath() {
        return fontPath;
    }

    public String getTitle() {
        return title;
    }

    public String getMessage() {
        return message;
    }

    public boolean isHorizontal() {
        return horizontal;
    }

    public boolean isCancelable() {
        return cancelable;
    }

    public String getIconName() {
        return iconName;
    }

    public List<ButtonProps> getButtons() {
        return buttons;
    }

    public boolean isHasInput() {
        return hasInput;
    }

    public String getInputHolder() {
        return inputHolder;
    }

    public String getKeyboardType() {
        return keyboardType;
    }

    public String getText() {
        return text;
    }

    public int getSeparatorHeight() {
        return separatorHeight;
    }

    public String getSeparatorColor() {
        return separatorColor;
    }

    public boolean isNoLastSeparator() {
        return noLastSeparator;
    }

    @Override
    public String toString() {
        return "DialogOption{" +
                "fontPath='" + fontPath + '\'' +
                ", title='" + title + '\'' +
                ", message='" + message + '\'' +
                ", horizontal=" + horizontal +
                ", cancelable=" + cancelable +
                ", iconName='" + iconName + '\'' +
                ", buttons=" + buttons +
                '}';
    }
}
