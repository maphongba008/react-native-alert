package com.maphongba008;

import com.facebook.react.bridge.ReadableMap;

/**
 * ReactNativeTemplate - com.reactlibrary
 * Created by Kerofrog on 2019-07-20.
 */
public class MapUtils {

    static int fromMap(ReadableMap map, String keyName, int defaultValue) {
        if (map == null) {
            return defaultValue;
        }
        if (!map.hasKey(keyName)) {
            return defaultValue;
        }
        return map.getInt(keyName);
    }

    static String fromMap(ReadableMap map, String keyName, String defaultValue) {
        if (map == null) {
            return defaultValue;
        }
        if (!map.hasKey(keyName)) {
            return defaultValue;
        }
        String str = map.getString(keyName);
        if (str == null) {
            return defaultValue;
        }
        return str;
    }

    static boolean fromMap(ReadableMap map, String keyName, boolean defaultValue) {
        if (map == null) {
            return defaultValue;
        }
        if (!map.hasKey(keyName)) {
            return defaultValue;
        }
        return map.getBoolean(keyName);
    }

}
