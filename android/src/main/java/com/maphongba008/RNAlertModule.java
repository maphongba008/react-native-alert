
package com.maphongba008;

import android.app.Activity;
import android.util.Log;

import com.facebook.react.bridge.Callback;
import com.facebook.react.bridge.ReactApplicationContext;
import com.facebook.react.bridge.ReactContextBaseJavaModule;
import com.facebook.react.bridge.ReactMethod;
import com.facebook.react.bridge.ReadableMap;

public class RNAlertModule extends ReactContextBaseJavaModule {

    private final ReactApplicationContext reactContext;

    public RNAlertModule(ReactApplicationContext reactContext) {
        super(reactContext);
        this.reactContext = reactContext;
    }

    @ReactMethod
    public void showAlert(ReadableMap map, Callback callback) {
        Activity activity = getCurrentActivity();
        AlertDialogFragment fragment = new AlertDialogFragment();

        DialogOption option = new DialogOption(map);
        Log.e("OPTION", option.toString());
        fragment.update(option, callback);
        if (activity != null) {
            fragment.show(activity.getFragmentManager(), "");
        }
    }

    @Override
    public String getName() {
        return "RNAlert";
    }
}