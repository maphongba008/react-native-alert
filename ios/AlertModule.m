//
//  AlertModule.m
//  acb_mobile_next
//
//  Created by MAC OS on 7/9/19.
//  Copyright © 2019 Facebook. All rights reserved.
//

#import "AlertModule.h"
#import <React/RCTUtils.h>
#import <React/RCTBridgeModule.h>

@implementation AlertModule

RCT_EXPORT_MODULE(RNAlert);

RCT_EXPORT_METHOD(showAlert: (NSDictionary *)config onPress: (RCTResponseSenderBlock) onPress)
{
  dispatch_sync(dispatch_get_main_queue(), ^{
    UIViewController *root = RCTPresentedViewController();
      NSString *path = [NSBundle.mainBundle pathForResource:@"Resources" ofType:@"bundle"];
      NSBundle *bundle = [NSBundle bundleWithPath:path];
      UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"MyAlert" bundle:bundle];
      AlertController *controller = (AlertController *)[storyboard instantiateViewControllerWithIdentifier:@"AlertController"];
    controller.config = config;
    controller.onPress = onPress;
    [root presentViewController:controller animated:YES completion:^{
      
    }];
      [controller doAnimation];
  });
}

@end
