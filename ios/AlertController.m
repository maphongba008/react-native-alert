//
//  AlertController.m
//  acb_mobile_next
//
//  Created by MAC OS on 7/9/19.
//  Copyright © 2019 Facebook. All rights reserved.
//

#import "AlertController.h"
#import "Color.h"

@interface AlertController ()

@property (nonatomic, weak) IBOutlet UIView *containerView;

@property (nonatomic, weak) IBOutlet UIImageView *icon;
@property (nonatomic, weak) IBOutlet UIView *buttonView;
@property (nonatomic, weak) IBOutlet NSLayoutConstraint *buttonViewHeight;
@property (nonatomic, weak) IBOutlet UILabel *titleText;
@property (nonatomic, weak) IBOutlet UILabel *messageText;
@property (nonatomic, weak) IBOutlet UIStackView *stackView;
@property (nonatomic, weak) IBOutlet UIView *titleSpacingView;
@property (nonatomic, weak) IBOutlet UITextField *inputField;
@property (nonatomic, weak) IBOutlet UIButton *closeButton;
@property (nonatomic, weak) IBOutlet NSLayoutConstraint *centerY;
@end

@implementation AlertController

static NSInteger const BUTTON_HEIGHT = 50;
static NSInteger const MARGIN = 16;

- (void)viewDidLoad {
    [super viewDidLoad];
    [self initUI];
}

- (void)viewWillAppear:(BOOL)animated {
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillShow:) name:UIKeyboardWillShowNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillHide:) name:UIKeyboardWillHideNotification object:nil];
}

- (NSString *) getStringFromDict: (NSDictionary *) dict andKey: (NSString *) key defaultValue: (NSString*) defaultValue {
    if ([dict valueForKey:key]) {
        return [dict valueForKey:key];
    }
    return defaultValue;
}

- (NSInteger) getIntFromDict: (NSDictionary *) dict andKey: (NSString *) key defaultValue: (NSInteger) defaultValue {
    if ([dict valueForKey:key]) {
        return [[dict valueForKey:key] integerValue];
    }
    return defaultValue;
}

- (BOOL) getBoolFromDict: (NSDictionary *) dict andKey: (NSString *) key {
    if ([dict valueForKey:key] == NULL) {
        return false;
    } else {
        return [[dict valueForKey:key] boolValue];
    }
}

- (void) initUI {
    self.containerView.layer.cornerRadius = 8;
    self.containerView.clipsToBounds = YES;
    NSString *iconName = [self.config valueForKey:@"iconName"];
    if (iconName) {
        [self.icon setImage:[UIImage imageNamed:iconName]];
    } else {
        NSString *path = [NSBundle.mainBundle pathForResource:@"Resources" ofType:@"bundle"];
        NSBundle *bundle = [NSBundle bundleWithPath:path];
        [self.icon setImage:[UIImage imageNamed:@"ic_info" inBundle:bundle compatibleWithTraitCollection:NULL]];
    }
    NSString *fontPath = [self.config valueForKey:@"fontPath"];
    
    NSString *title = (NSString *)[self.config valueForKey:@"title"];
    if (title) {
        self.titleText.text = title;
        if (fontPath) {
            self.titleText.font = [UIFont fontWithName:fontPath size:18];
        }
    } else {
        [self.titleText removeFromSuperview];
        [self.titleSpacingView removeFromSuperview];
    }
    NSString *message = (NSString *)[self.config valueForKey:@"message"];
    self.messageText.text = message;
    if (fontPath) {
        self.messageText.font = [UIFont fontWithName:fontPath size:16];
    }
    if (![self getBoolFromDict:self.config andKey:@"cancelable"]) {
        [self.closeButton removeFromSuperview];
    }
    
    NSArray *buttons = (NSArray *)[self.config valueForKey:@"buttons"];
    NSDictionary *option = self.config;
    
    if ([self getBoolFromDict:_config andKey:@"hasInput"]) {
        self.inputField.placeholder = [self.config valueForKey:@"inputHolder"];
        self.inputField.text = [self.config valueForKey:@"text"];
        self.inputField.leftView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 8, 0)];
        self.inputField.leftViewMode = UITextFieldViewModeAlways;
        self.inputField.layer.borderColor = [Color fromString:@"#e4e4e4"].CGColor;
        self.inputField.layer.borderWidth = 1;
        if (fontPath) {
            self.inputField.font = [UIFont fontWithName:fontPath size:14];
        }
        NSString *keyboardType = [self.config valueForKey:@"keyboardType"];
        if ([keyboardType isEqualToString:@"number"]) {
            [self.inputField setKeyboardType:UIKeyboardTypeNumberPad];
        } else if ([keyboardType isEqualToString:@"password"]) {
            [self.inputField setKeyboardType:UIKeyboardTypeDefault];
            self.inputField.secureTextEntry = YES;
        } else {
            [self.inputField setKeyboardType:UIKeyboardTypeDefault];
        }
    } else {
        [self.inputField removeFromSuperview];
    }
    BOOL horizontal = NO;
    if ([option valueForKey:@"horizontal"] == NULL) {
        horizontal = buttons.count < 3;
    } else {
        horizontal = [[option valueForKey:@"horizontal"] boolValue];
    }
    if (horizontal) {
        [self initHorizontalButtons];
    } else {
        [self initVerticalButtons];
    }
}

- (void) onButtonClicked: (UIButton *) sender {
    
    [self dismissSelf];
    NSString *text = self.inputField.text ? self.inputField.text : @"";
    self.onPress(@[[NSNumber numberWithInt:(int) sender.tag], text]);
}

- (void) initHorizontalButtons {
    NSArray *buttons = (NSArray *)[self.config valueForKey:@"buttons"];
    NSInteger numberOfButtons = buttons.count;
    CGFloat buttonWidth = ((self.view.bounds.size.width - MARGIN * 2) - (numberOfButtons - 1)) / numberOfButtons;
     [self.buttonViewHeight setConstant:(BUTTON_HEIGHT + 1)];
    for (int i = 0; i < numberOfButtons; i++) {
        NSDictionary *buttonConfig = (NSDictionary *)buttons[i];
        UIButton *button = [[UIButton alloc] initWithFrame:CGRectMake(i * buttonWidth + i * 1, 1, buttonWidth, BUTTON_HEIGHT)];
        button.tag = i;
        [self setUIButton:buttonConfig button:button];
        [self.buttonView addSubview:button];
    }
}

- (void) initVerticalButtons {
    NSString *separatorColor = [self getStringFromDict:self.config andKey:@"separatorColor" defaultValue:NULL];
    NSInteger separatorHeight = [self getIntFromDict:self.config andKey:@"separatorHeight" defaultValue:1];
    BOOL noLastSeparator = [self getBoolFromDict:self.config andKey:@"noLastSeparator"];
    
    NSArray *buttons = (NSArray *)[self.config valueForKey:@"buttons"];
    NSInteger numberOfButtons = buttons.count;
    
    CGFloat buttonWidth = self.view.frame.size.width - MARGIN * 2;
//    [self.buttonViewHeight setConstant:(numberOfButtons * BUTTON_HEIGHT + (numberOfButtons * separatorHeight))];
    /**
     private int marginHorizontal;
     private String backgroundColor;
     private int borderRadius;
     private int height;
**/
    NSInteger startY = 0;
    for (int i = 0; i < numberOfButtons; i++) {
        NSDictionary *buttonConfig = (NSDictionary *)buttons[i];
        
        NSInteger buttonHeight = [self getIntFromDict:buttonConfig andKey:@"height" defaultValue:50];
        NSString *backgroundColor = [self getStringFromDict:buttonConfig andKey:@"backgroundColor" defaultValue:NULL];
        NSInteger marginHorizontal = [self getIntFromDict:buttonConfig andKey:@"marginHorizontal" defaultValue:0];
        NSInteger borderRadius = [self getIntFromDict:buttonConfig andKey:@"borderRadius" defaultValue:0];

        if (!noLastSeparator || i != numberOfButtons - 1) {
            UIView *separator = [[UIView alloc] initWithFrame:CGRectMake(0, startY, buttonWidth, separatorHeight)];
            separator.backgroundColor = [Color fromString:separatorColor];
            [self.buttonView addSubview:separator];
            startY += separatorHeight;
        }
        
        UIButton *button = [[UIButton alloc] initWithFrame:CGRectMake(marginHorizontal, startY, buttonWidth - marginHorizontal * 2, buttonHeight)];
        if (borderRadius > 0) {
            button.layer.cornerRadius = borderRadius;
        }
        button.backgroundColor = [Color fromString:backgroundColor];
        button.tag = i;
        [self setUIButton:buttonConfig button:button];
        [self.buttonView addSubview:button];
        startY += buttonHeight;
    }
    self.buttonViewHeight.constant = startY;
    
}

- (void) setUIButton: (NSDictionary *) buttonConfig button: (UIButton *) button {
    NSString *fontPath = [self.config valueForKey:@"fontPath"];
    NSString *buttonFontPath = [buttonConfig valueForKey:@"fontPath"];
//    button.backgroundColor = [UIColor whiteColor];
    [button setTitle:(NSString *)[buttonConfig valueForKey:@"text"] forState:UIControlStateNormal];
  
    [button addTarget:self action:@selector(onButtonClicked:) forControlEvents: UIControlEventTouchUpInside];
    
    if (buttonFontPath) {
        [button.titleLabel setFont:[UIFont fontWithName:buttonFontPath size:17]];
    } else if (fontPath) {
        [button.titleLabel setFont:[UIFont fontWithName:fontPath size:17]];
    } else {
        [button.titleLabel setFont:[UIFont fontWithName:@"System" size:17]];
    }
    
    NSString *textColor = [buttonConfig valueForKey:@"textColor"];
    if (textColor) {
          [button setTitleColor:[Color fromString:textColor] forState:UIControlStateNormal];
    } else {
          [button setTitleColor:[UIColor colorWithRed:22.0/255 green:63.0/255 blue:161.0/255 alpha:1] forState:UIControlStateNormal];
    }
    
}

- (void) doAnimation {
    [self.view setBackgroundColor:[UIColor clearColor]];
    [UIView animateWithDuration:0.15 animations:^{
        [self.view setBackgroundColor:[UIColor colorWithRed:0 green:0 blue:0 alpha:0.3]];
    } completion:^(BOOL finished) {
        
    }];
}

- (IBAction)closeButtonClicked:(id)sender {
    [self dismissSelf];
}

- (void) dismissSelf {
    [self dismissViewControllerAnimated:YES completion:^{
        
    }];
}

- (void)keyboardWillShow:(NSNotification *)notification {
    NSDictionary* keyboardInfo = [notification userInfo];
    NSValue* keyboardFrameBegin = [keyboardInfo valueForKey:UIKeyboardFrameBeginUserInfoKey];
    CGRect rect = [keyboardFrameBegin CGRectValue];
    NSInteger moveUp = (self.view.frame.size.height - self.containerView.frame.size.height - rect.size.height) / 2;
    self.centerY.constant = 0;
    [UIView animateWithDuration:1 animations:^{
        [self.centerY setConstant:-moveUp];
        [self.view layoutSubviews];
    }];
}


- (void)keyboardWillHide:(NSNotification *)notification {
    
}

- (void)viewWillDisappear:(BOOL)animated {
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIKeyboardWillShowNotification object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIKeyboardWillHideNotification object:nil];
}

@end
