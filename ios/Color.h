//
//  Color.h
//  RNReactNativeAlert
//
//  Created by MAC OS on 7/22/19.
//  Copyright © 2019 Facebook. All rights reserved.
//
#import <UIKit/UIKit.h>
#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface Color : NSObject
+ (UIColor *) fromString: (NSString *) hexStr;
@end

NS_ASSUME_NONNULL_END
