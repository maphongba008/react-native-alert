//
//  Color.m
//  RNReactNativeAlert
//
//  Created by MAC OS on 7/22/19.
//  Copyright © 2019 Facebook. All rights reserved.
//

#import "Color.h"

@implementation Color

+ (unsigned int)intFromHexString:(NSString *)hexStr
{
    unsigned int hexInt = 0;
    
    // Create scanner
    NSScanner *scanner = [NSScanner scannerWithString:hexStr];
    
    // Tell scanner to skip the # character
    [scanner setCharactersToBeSkipped:[NSCharacterSet characterSetWithCharactersInString:@"#"]];
    
    // Scan hex value
    [scanner scanHexInt:&hexInt];
    
    return hexInt;
}

+ (UIColor *) fromString: (NSString *) hexStr {
    if ([hexStr isKindOfClass:[NSNull class]] || hexStr == NULL) {
        return UIColor.clearColor;
    }
    // Convert hex string to an integer
    unsigned int hexint = [self intFromHexString:hexStr];
    
    // Create a color object, specifying alpha as well
    UIColor *color =
    [UIColor colorWithRed:((CGFloat) ((hexint & 0xFF0000) >> 16))/255
                    green:((CGFloat) ((hexint & 0xFF00) >> 8))/255
                     blue:((CGFloat) (hexint & 0xFF))/255
                    alpha:1];
    
    return color;
}


@end
