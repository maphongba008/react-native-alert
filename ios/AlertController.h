//
//  AlertController.h
//  acb_mobile_next
//
//  Created by MAC OS on 7/9/19.
//  Copyright © 2019 Facebook. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <React/RCTBridgeModule.h>

NS_ASSUME_NONNULL_BEGIN

@interface AlertController : UIViewController

@property  (nonatomic, strong) NSDictionary *config;
@property  (nonatomic, strong) RCTResponseSenderBlock onPress;
- (void) doAnimation;
@end

NS_ASSUME_NONNULL_END
